module.exports = {
    env: {
        browser: true,
        es2021: true
    },
    extends: [
        "plugin:react/recommended",
        "airbnb",
        "plugin:@typescript-eslint/recommended",
        "plugin:import/typescript"
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        ecmaVersion: 12,
        sourceType: "module"
    },
    plugins: ["react", "@typescript-eslint"],
    rules: {
        indent: ["error", 4],
        semi: ["error", "never"],
        quotes: ["error", "double"],
        "object-curly-newline": 0,
        "brace-style": 0,
        "arrow-parens": ["error", "as-needed"],
        "arrow-body-style": 0,
        "react/jsx-props-no-spreading": 0,
        "react/jsx-first-prop-new-line": 0,
        "react/jsx-max-props-per-line": 0,
        "react/jsx-indent-props": 0,
        "eol-last": 0,
        "comma-dangle": ["error", "never"],
        "react/prop-types": 0,
        "object-shorthand": 0,
        "no-lonely-if": 0,
        "react/no-unescaped-entities": 0,
        "no-use-before-define": "off",
        "no-return-assign": "off",
        "no-confusing-arrow": "off",
        "import/prefer-default-export": "off",
        "@typescript-eslint/no-inferrable-types": "off",
        "@typescript-eslint/no-use-before-define": ["error"],
        "@typescript-eslint/no-empty-function": "off",
        "no-mixed-operators": "off",
        "no-console": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "react/jsx-curly-newline": "off",
        "react/jsx-wrap-multilines": "off",
        "operator-linebreak": "off",
        "object-property-newline": "off",
        "jsx-a11y/label-has-associated-control": "off",
        "no-restricted-syntax": "off",
        "guard-for-in": "off",
        "jsx-a11y/no-autofocus": 0,
        "no-return-await": 0,
        "react/no-array-index-key": 0,
        "jsx-a11y/no-static-element-interactions": 0,
        "jsx-a11y/click-events-have-key-events": 0,
        "react/button-has-type": 0,
        "no-underscore-dangle": 0,
        "no-unused-vars": 0,
        "no-non-null-assertion": 0,
        "@typescript-eslint/no-unused-vars": 0,
        "@typescript-eslint/no-non-null-assertion": 0,
        "no-else-return": 0,
        "global-require": 0,
        "@typescript-eslint/no-var-requires": 0,
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                js: "never",
                jsx: "never",
                ts: "never",
                tsx: "never"
            }
        ],
        "no-param-reassign": ["error", { props: false }],
        "max-len": 0,
        "react/jsx-indent": 0,
        "@typescript-eslint/explicit-function-return-type": "off",
        "react/jsx-filename-extension": [
            2,
            {
                extensions: [".js", ".jsx", ".ts", ".tsx"]
            }
        ],
        "react/jsx-one-expression-per-line": 0,
        "import/no-extraneous-dependencies": 0,
        "import/no-unresolved": 0,
        "@typescript-eslint/explicit-module-boundary-types": 0,
        "no-restricted-globals": "off"
    }
}
