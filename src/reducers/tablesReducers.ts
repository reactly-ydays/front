/* eslint-disable no-plusplus */
/* eslint-disable no-case-declarations */
/* eslint-disable prefer-const */
import { TaskType, SubTableType, TableType } from "../models/Types"

export const initState = {
    tables: []
}

export const tablesReducer = (state: any, action: any) => {
    switch (action.type) {
    case "INIT_TABLE":
        return {
            ...state,
            tables: action.payload
        }
    case "ADD_TABLE":
        return {
            ...state,
            tables: [...state.tables, action.payload]
        }
    case "EDIT_TABLE":
        let arrTmp8 = state.tables

        arrTmp8[action.payload.tableIndex].name = action.payload.name

        return {
            ...state,
            tables: arrTmp8
        }
    case "DELETE_TABLE":
        let arrTmp9 = state.tables

        const arrTemp = arrTmp9.filter((table: TableType) => table._id !== action.payload.tableId)

        return {
            ...state,
            tables: arrTemp
        }
    case "ADD_SUB_TABLE":
        let arrTmp1 = state.tables
        arrTmp1[action.payload.tableIndex].subTable.push(action.payload)

        return {
            ...state,
            tables: arrTmp1
        }
    case "EDIT_SUB_TABLE":
        let arrTmp5 = state.tables

        const stTmpIndex = arrTmp5[action.payload.tableIndex].subTable.findIndex((subTable: SubTableType) => subTable._id === action.payload._id)
        arrTmp5[action.payload.tableIndex].subTable[stTmpIndex] = action.payload

        return {
            ...state,
            tables: arrTmp5
        }
    case "DELETE_SUB_TABLE":
        let arrTmp4 = state.tables

        const tkTmp = arrTmp4[action.payload.tableIndex].tasks.filter((task: TaskType) => task.subTableId !== action.payload.subTableId)
        const stTmp = arrTmp4[action.payload.tableIndex].subTable.filter((subTable: SubTableType) => subTable._id !== action.payload.subTableId)
        arrTmp4[action.payload.tableIndex].tasks = tkTmp
        arrTmp4[action.payload.tableIndex].subTable = stTmp

        return {
            ...state,
            tables: arrTmp4
        }
    case "ADD_TASK":
        const { _id, title, description, subTableId, user, priority, label, labelColor, estimate } = action.payload
        let arrTmp2 = state.tables
        arrTmp2[action.payload.tableIndex].tasks.push({ _id, title, description, subTableId, user, priority, label, labelColor, estimate })

        return {
            ...state,
            tables: arrTmp2
        }
    case "MOVE_TASK":
        let arrTmp3 = state.tables
        const index = arrTmp3[action.payload.tableIndex].tasks.findIndex((t: TaskType) => t._id === action.payload.task._id)
        arrTmp3[action.payload.tableIndex].tasks[index].subTableId = action.payload.subTableId

        return {
            ...state,
            tables: arrTmp3
        }
    case "EDIT_TASK":
        let arrTmp6 = state.tables

        const taskTmpIndex = arrTmp6[action.payload.tableIndex].tasks.findIndex((t: TaskType) => t._id === action.payload.task._id)
        arrTmp6[action.payload.tableIndex].tasks[taskTmpIndex] = action.payload.task

        return {
            ...state,
            tables: arrTmp6
        }
    case "DELETE_TASK":
        let arrTmp7 = state.tables

        const tmpTask = arrTmp7[action.payload.tableIndex].tasks.filter((t: TaskType) => t._id !== action.payload.taskId)
        arrTmp7[action.payload.tableIndex].tasks = tmpTask

        return {
            ...state,
            tables: arrTmp7
        }
    default:
        return initState
    }
}
