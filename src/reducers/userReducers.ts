export const initState = {
    user: null
}

export const userReducer = (state: any, action: any) => {
    switch (action.type) {
    case "ADD_USER":
        return {
            ...state,
            user: action.payload
        }
    default:
        return initState
    }
}
