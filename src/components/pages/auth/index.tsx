import React, { useContext, useEffect } from "react"
import Login from "./includes/Login"
import Register from "./includes/Register"
import { PostsContext } from "../../../context/postsContext"
import { useWindowResolution } from "../../../hooks/CustomHooks"

type PropsType = {
    location: any
}

const Auth: React.FC<PropsType> = ({ location }) => {
    const { width } = useWindowResolution()

    document.body.style.backgroundColor = (width <= 640) ? "#FFFFFF" : "#F1F1F1"

    return (
        <div>
            <h1 className="mt-4 sm:my-5 text-3xl md:text-4xl font-bold text-center text-blue-600">Reactly</h1>
            {location.pathname === "/login" && <Login />}
            {location.pathname === "/register" && <Register />}
        </div>
    )
}

export default Auth