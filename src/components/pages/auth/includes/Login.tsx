import React, { useContext, useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { Link, Redirect } from "react-router-dom"
import { getUser } from "../../../../api/ApiRequest"
import { userAuth, checkUserAuth } from "../../../../api/auth"
import { UserContext } from "../../../../context/userContext"
import Loading from "../../../../utils/Loading"
import { loginRequest } from "./Requests"

const Login: React.FC = () => {
    const [email, setEmail] = useState<string>("")
    const [password, setPassword] = useState<string>("")
    const [isLogin, setIsLogin] = useState<boolean>(false)
    const [isError, setIsError] = useState<boolean>(false)
    const [errorMsg, setErrorMsg] = useState<string>("")
    const [isSubmit, setIsSubmit] = useState<boolean>(false)
    const { userState, userDispatch } = useContext(UserContext)
    const { register, errors, handleSubmit } = useForm()

    useEffect(() => {
        if (checkUserAuth()) {
            setIsLogin(true)
        }
    }, [])

    const handlerChangeValue = (e: any) => {
        const { name, value } = e.target
        switch (name) {
        case "email":
            setEmail(value)
            break
        case "password":
            setPassword(value)
            break
        default:
            break
        }
        setIsError(false)
        setErrorMsg("")
    }

    const onSubmit = () => {
        setIsSubmit(true)
        loginRequest(email, password).then(res => {
            if (res.data.status === "success") {
                userAuth(res.data.data.accessToken, res.data.data.refreshToken)
                setIsLogin(true)
            } else {
                setIsError(true)
                setErrorMsg(res.data.message)
                setIsSubmit(false)
            }
        }).catch(err => {
            setIsError(true)
            setErrorMsg(err.message)
            setIsSubmit(false)
        })
    }

    if (isLogin) {
        getUser().then(res => {
            userDispatch({
                type: "ADD_USER",
                payload: res.data.data
            })
        })

        return (
            <Redirect to="/timelines" />
        )
    }

    return (
        <div className="sm:container sm:w-auth px-2 sm:py-0.5 sm:bg-white-normal sm:shadow-default sm:border sm:border-gray-200 sm:rounded-xl">
            <p className="text-center my-0.5 sm:my-2 font-medium text-lg">Se connecter à Reactly</p>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="auth-field-container">
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="email"
                        name="email"
                        value={email}
                        autoFocus
                        onChange={handlerChangeValue}
                        placeholder="Adresse e-mail"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.email && "#f02849" }}

                    />
                    {errors.email && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un email</p>}
                </div>
                <div className="auth-field-container">
                    <input
                        className="w-full bg-blue-100 sm:bg-white-normal my-1 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                        type="password"
                        name="password"
                        value={password}
                        onChange={handlerChangeValue}
                        placeholder="Mot de passe"
                        ref={register && register({ required: true })}
                        style={{ borderColor: errors.password && "#f02849" }}
                    />
                    {errors.password && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un mot de passe</p>}
                </div>
                <input className="w-full text-white-normal bg-blue-600 rounded-md font-bold text-xl py-1.5 my-0.5 cursor-pointer" type="submit" value="Connexion" />
                {isError && <p className="w-full text-center py-1.5 px-0.5 mt-0.5 rounded-md text-red-error bg-red-light">{errorMsg}</p>}
                <div className="flex justify-center items-center m-3">
                    <p className="text-lg sm:text-base">Pas encore de compte ? </p>
                    <Link to="/register" className="ml-1 font-bold text-green-500 text-lg sm:text-base">S'inscrire</Link>
                </div>

            </form>
            {isSubmit && <Loading />}
        </div>
    )
}

export default Login