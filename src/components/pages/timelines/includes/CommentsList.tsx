import React, { useContext, useState } from "react"
import TextareaAutosize from "react-textarea-autosize"
import { useForm } from "react-hook-form"
import { UserContext } from "../../../../context/userContext"
import { CommentType } from "../../../../models/Types"
import Comment from "./Comment"
import SendIcon from "../../../../assets/img/posts/ic_send.svg"
import { newComment } from "./Requests"
import { getAllPost } from "../../../../api/ApiRequest"
import { PostsContext } from "../../../../context/postsContext"

type PropsType = {
    comments: Array<CommentType>,
    postId: string
}

const CommentsList: React.FC<PropsType> = ({ comments, postId }) => {
    const { userState, userDispatch } = useContext(UserContext)
    const { postsState, postsDispatch } = useContext(PostsContext)
    const { register, errors, handleSubmit, setValue, getValues } = useForm()

    const onSubmit = () => {
        newComment(getValues("comment"), postId).then(res => {
            postsDispatch({
                type: "UPDATE_POST",
                payload: {
                    postId,
                    post: res.data.data
                }
            })
            setValue("comment", "")
        })
        setValue("comment", "")
    }

    return (
        <div>
            {
                comments.map((item, index) => <Comment key={index} comment={item} />)
            }
            <form onSubmit={handleSubmit(onSubmit)} className={`flex w-full mt-1.5 ${comments.length ? "border-t pt-1.5" : ""} border-gray-300`}>
                <img src={`${userState.user?.image}`} className="w-9 h-9 rounded-full mr-1" alt="user profil" width="30" height="30" />
                <TextareaAutosize
                    rows={1}
                    name="comment"
                    className="w-full p-1 text-md bg-blue-100 sm:bg-gray-100 border border-gray-300 sm:border-gray-50 sm:border-2 rounded-md font-medium outline-none resize-none"
                    placeholder={`Que voulez-vous dire, ${userState.user?.firstName} ?`}
                    ref={register && register({ required: true })}
                />
                <button type="submit" className="w-10 h-9 bg-blue-600 rounded-md ml-1">
                    <img src={SendIcon} className="m-auto" alt="user profil" width="20" height="20" />
                </button>
            </form>
        </div>
    )
}

export default CommentsList