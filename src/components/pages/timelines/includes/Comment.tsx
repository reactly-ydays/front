import React from "react"
import DescriptionSection from "./DescriptionSection"
import { capitalize, dateFormat } from "../../../../utils/Utils"
import { CommentType } from "../../../../models/Types"

type PropsType = {
    comment: CommentType
}

const newlineText = (text: string) => {
    const regexUrl = /(https?:\/\/[^\s]+)/g
    let parts: Array<any> = []
    if (text.match(regexUrl)) {
        parts = text.split(regexUrl)
        for (let i = 1; i < parts.length; i += 2) {
            parts[i] = <a key={`link${i}`} className="text-blue-600 hover:underline" href={parts[i]} target="_blank" rel="noreferrer">{parts[i]}</a>
        }
        return parts
    }
    return text
}

const Comment: React.FC<PropsType> = ({ comment }) => {
    return (
        <div>
            <div className="flex w-full mt-1.5">
                <img src={`${comment.user?.image}`} className="w-9 h-9 rounded-full" alt="user profil" width="30" height="30" />
                <div className="flex-col ml-1 item-center bg-gray-200 bg-opacity-40 rounded-xl px-1.2 py-1">
                    <div className="flex items-center">
                        <h3 className="text-gray-500">{capitalize(comment.user?.name)}</h3>
                        <p className="ml-1 text-sm text-gray-400">- {dateFormat(new Date(comment.publicationDate))}</p>
                    </div>
                    <p className="text-md whitespace-pre-wrap">{newlineText(comment.comment)}</p>
                </div>
            </div>
        </div>
    )
}

export default Comment