/* eslint-disable react/self-closing-comp */
import React, { useCallback, useEffect, useRef, useState } from "react"
import Gallery from "react-photo-gallery"
import Carousel, { Modal, ModalGateway } from "react-images"
import useSmoothScroll from "react-smooth-scroll-hook"
import LeftArrowIcon from "../../../../assets/img/posts/ic_left_arrow.svg"
import RightArrowIcon from "../../../../assets/img/posts/ic_right_arrow.svg"
import "../../../../assets/css/overlay-render.css"

const regexUrl = /(https?:\/\/[^\s]+)/g

type Props = {
    description: string,
    images?: string[]
}

const newlineText = (text: string) => {
    let parts: Array<any> = []
    if (text.match(regexUrl)) {
        parts = text.split(regexUrl)
        for (let i = 1; i < parts.length; i += 2) {
            parts[i] = <a key={`link${i}`} className="text-blue-600 hover:underline" href={parts[i]} target="_blank" rel="noreferrer">{parts[i]}</a>
        }
        return parts
    }
    return text
}

const DescriptionSection: React.FC<Props> = ({ description, images }) => {
    const [selectedImage, setSelectedImage] = useState<any>([])
    const [currentImage, setCurrentImage] = useState(0)
    const [viewerIsOpen, setViewerIsOpen] = useState(false)
    const ref = useRef(null)
    const { scrollTo } = useSmoothScroll({
        ref,
        speed: 15,
        direction: "x"
    })

    useEffect(() => {
        if (images?.length) {
            images?.forEach((item: string) => {
                setSelectedImage((prevImages: any) => prevImages.concat({
                    src: item,
                    width: 1,
                    height: 1
                }))
            })
        }
    }, [])

    const openLightbox = useCallback((event, { photo, index }) => {
        setCurrentImage(index)
        setViewerIsOpen(true)
    }, [])

    const closeLightbox = () => {
        setCurrentImage(0)
        setViewerIsOpen(false)
    }

    const ImageRender = ({ index, onClick, photo, margin }: any) => (
        <div>
            <div className="content cursor-pointer">
                <div style={{ margin, width: photo.width, height: "150px", backgroundImage: `url(${photo.src})`, backgroundSize: "cover" }} onClick={e => onClick(e, { index, photo })}>
                    <span></span>
                </div>
            </div>
        </div>
    )

    return (
        <div className="mx-1 mt-1.5 mb-3">
            <p className="text-lg whitespace-pre-wrap">{newlineText(description)}</p>
            {images !== null ? (
                <div className="mt-1">
                    {/* Gallery images */}
                    <div>
                        {selectedImage.length > 0 ? (<p className="text-sm text-gray-600">{selectedImage.length} image{selectedImage.length > 1 ? "s" : ""}</p>) : null}
                        <div className="flex">
                            {selectedImage.length > 2 ? (
                                <div className="flex cursor-pointer justify-center" onClick={() => scrollTo(-340)}>
                                    <img src={LeftArrowIcon} className="w-9/12" alt="left arrow" />
                                </div>
                            ) : null}

                            <div className={`${selectedImage.length > 2 ? "w-11/12" : "w-full"}`}>
                                <div style={{ overflowX: "scroll" }} className="overflow-hidden mb-1.5" ref={ref}>
                                    <Gallery photos={selectedImage} columns={selectedImage.length > 2 ? 2 : selectedImage.length} onClick={openLightbox} ImageComponent={ImageRender} />
                                </div>
                                <ModalGateway>
                                    {viewerIsOpen ? (
                                        <Modal onClose={closeLightbox}>
                                            <Carousel
                                                currentIndex={currentImage}
                                                views={selectedImage.map((x: any) => ({
                                                    ...x
                                                }))}
                                            />
                                        </Modal>
                                    ) : null}
                                </ModalGateway>
                            </div>
                            {selectedImage.length > 2 ? (
                                <div className="flex cursor-pointer justify-center" onClick={() => scrollTo(340)}>
                                    <img src={RightArrowIcon} className="w-9/12" alt="right arrow" />
                                </div>
                            ) : null}
                        </div>
                    </div>
                </div>) : null}
        </div>
    )
}

export default DescriptionSection
