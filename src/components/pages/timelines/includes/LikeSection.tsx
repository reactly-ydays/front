import React, { useEffect, useState } from "react"
import { CopyToClipboard } from "react-copy-to-clipboard"
import { APP_BASE_URL } from "../../../../utils/Utils"
import LikeIcon from "../../../../assets/img/home/ic_like.svg"
import LikeIconActive from "../../../../assets/img/home/ic_like_active.svg"
import CommentIcon from "../../../../assets/img/home/ic_comment.svg"
import CommentIconActive from "../../../../assets/img/home/ic_comment_active.svg"
import ShareIcon from "../../../../assets/img/home/ic_share.svg"
import { notificationAlert } from "../../../../utils/Notification"

type Props = {
    isLiked: boolean,
    isOpenComment: boolean,
    nbComment: number,
    handlerLikeBtn: any,
    handlerOpenComment: any,
    postId: string
}

const LikeSection: React.FC<Props> = ({ isLiked, isOpenComment, nbComment, handlerLikeBtn, handlerOpenComment, postId }) => {
    const [copied, setCopied] = useState<boolean>(false)

    useEffect(() => {
        if (copied) {
            notificationAlert("📋 Le lien du post a été copié !", "info")
            setCopied(false)
        }
    }, [copied])

    return (
        <div className={`flex justify-around pt-1.5 border-t border-gray-300 ${isOpenComment ? "border-b pb-1.5" : ""}`}>
            <div className="flex items-center cursor-pointer" onClick={handlerLikeBtn}>
                <img src={isLiked ? LikeIconActive : LikeIcon} className="w-5 h-5 sm:w-6 sm:h-6" alt="like" />
                <p className={`font-medium ml-1 ${isLiked ? "text-red-like" : "text-gray-400"}`}>J'aime</p>
            </div>

            <div className="flex items-center cursor-pointer" onClick={handlerOpenComment}>
                <img src={isOpenComment ? CommentIconActive : CommentIcon} alt="like" className="w-5 h-5 sm:w-6 sm:h-6" />
                <p className={`font-medium ml-1 ${isOpenComment ? "text-blue-600" : "text-gray-400"}`}>{nbComment} Commentaire{nbComment > 1 ? "s" : ""}</p>
            </div>

            <CopyToClipboard text={`${APP_BASE_URL}/timelines/post/${postId}`} onCopy={() => setCopied(true)}>
                <div className="flex items-center cursor-pointer">
                    <img src={ShareIcon} alt="like" className="w-5 h-5" />
                    <p className="font-medium ml-1 text-gray-400">Partager</p>
                </div>
            </CopyToClipboard>
        </div>
    )
}

export default LikeSection
