import React, { useContext } from "react"
import { UserContext } from "../../../../context/userContext"
import LikeIcon from "../../../../assets/img/home/ic_like.svg"
import DeleteIcon from "../../../../assets/img/posts/ic_delete_black.svg"
import LikeIconActive from "../../../../assets/img/home/ic_like_active.svg"
import { PostType } from "../../../../models/Types"
import { dateFormat } from "../../../../utils/Utils"

type Props = {
    post: PostType,
    isLiked: boolean,
    nbLike: number,
    handlerLikeBtn: any,
    handlerDeleteBtn: any
}

const HeaderSection: React.FC<Props> = ({ post, isLiked, nbLike, handlerLikeBtn, handlerDeleteBtn }) => {
    const { userState, userDispatch } = useContext(UserContext)
    return (
        <div className="flex justify-between bg-white-normal items-center">
            <div className="flex justify-between">
                <img src={`${post.user.image}`} className="w-9 h-9 sm:w-10 sm:h-10 rounded-full" alt="user profil" width="40" height="40" />
                <div className="flex flex-col ml-1 leading-5">
                    <div className="flex">
                        <h3 className="text-gray-500">{post.user.name}</h3>
                        <p className="ml-1 text-sm text-gray-400">- {dateFormat(new Date(post.publicationDate))}</p>
                    </div>
                    <p className="text-gray-400">{post.user.job}</p>
                </div>
            </div>

            <div className="flex">
                <div className="flex justify-between cursor-pointer" onClick={handlerLikeBtn}>
                    <p className="mr-1 text-gray-500">{nbLike}</p>
                    <img src={isLiked ? LikeIconActive : LikeIcon} alt="like" width="20px" />
                </div>
                {post.user.id === userState.user?._id ? (
                    <div className="flex justify-between cursor-pointer ml-1" onClick={handlerDeleteBtn}>
                        <img src={DeleteIcon} alt="delete" width="21px" />
                    </div>
                ) : null}
            </div>
        </div>
    )
}

export default HeaderSection
