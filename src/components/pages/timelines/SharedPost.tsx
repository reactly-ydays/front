/* eslint-disable react/jsx-boolean-value */
import React, { useState } from "react"
import { getPost } from "../../../api/ApiRequest"
import { checkUserAuth } from "../../../api/auth"
import PublishedPost from "./includes/PublishedPost"
import { PostType } from "../../../models/Types"
import { useComponentWillMount } from "../../../utils/Utils"
import Loading from "../../../utils/Loading"
import { useWindowResolution } from "../../../hooks/CustomHooks"

type Props = {
    match: any
}

const SharedPost: React.FC<Props> = ({ match }) => {
    const { postId } = match.params
    const [post, setPost] = useState<PostType|null>(null)
    const { width } = useWindowResolution()
    const [isLoading, setIsLoading] = useState<boolean>(true)

    document.body.style.backgroundColor = (width <= 640) ? "#e0e0e0" : "#F1F1F1"

    useComponentWillMount(async () => {
        if (isLoading) {
            if (checkUserAuth()) {
                if (postId !== null) {
                    await getPost(postId).then(res => {
                        if (res.data.status === "success") {
                            setPost(res.data.data)
                        } else {
                            window.location.href = "/timelines"
                        }
                    }).catch(err => {
                        window.location.href = "/timelines"
                    })
                }
            }
            setIsLoading(false)
        }
    })

    if (isLoading) return (<Loading />)
    else if (post !== null) {
        return (
            <div className="container mx-auto sm:w-2/3 md:w-4/6 lg:w-2/5 lg:pt-7 pb-7">
                {(width > 640) && <h2 className="pt-12 mb-1 ml-1.5 sm:ml-0 text-lg lg:text-2xl font-bold text-gray-500">Actualité</h2>}
                <div className="pt-9 sm:pt-0">
                    <PublishedPost post={post} shared={true} />
                </div>
            </div>
        )
    }
    else return <div />
}

export default SharedPost
