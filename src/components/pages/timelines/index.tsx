import React, { useContext, useEffect, useState } from "react"
import { getAllPost } from "../../../api/ApiRequest"
import { checkUserAuth } from "../../../api/auth"
import { PostsContext } from "../../../context/postsContext"
import { useWindowResolution } from "../../../hooks/CustomHooks"
import Loading from "../../../utils/Loading"
import { useComponentWillMount } from "../../../utils/Utils"
import Actualite from "./includes/Actualite"
import NoActualite from "./includes/NoActualite"

const Timelines: React.FC = () => {
    const { postsState, postsDispatch } = useContext(PostsContext)
    const { width } = useWindowResolution()
    const [pagination, setPagination] = useState<number>(0)
    const [isLoading, setIsLoading] = useState<boolean>(true)

    document.body.style.backgroundColor = (width <= 640) ? "#e0e0e0" : "#F1F1F1"

    useComponentWillMount(async () => {
        if (isLoading) {
            if (checkUserAuth()) {
                if (postsState.posts === null) {
                    await getAllPost(pagination).then(res => {
                        if (res.data.hasNextPage) {
                            postsDispatch({
                                type: "HAS_NEXT_PAGE",
                                payload: true
                            })
                        }
                        postsDispatch({
                            type: "INIT_POST",
                            payload: res.data.data
                        })
                    })
                }
            }
            setIsLoading(false)
        }
    })

    useEffect(() => {
        if (postsState.loadMorePost) {
            getAllPost(pagination + 1).then(res => {
                if (res.data.hasNextPage) {
                    postsDispatch({
                        type: "HAS_NEXT_PAGE",
                        payload: true
                    })
                } else {
                    postsDispatch({
                        type: "HAS_NEXT_PAGE",
                        payload: false
                    })
                }
                postsDispatch({
                    type: "UPDATE_POSTS",
                    payload: res.data.data
                })
                postsDispatch({
                    type: "LOAD_MORE_POST",
                    payload: false
                })
                setPagination(pagination + 1)
            })
        }
    }, [postsState.loadMorePost])

    if (isLoading) return (<Loading />)
    else if (postsState.posts?.length) return <Actualite />
    else return <NoActualite />
}

export default Timelines
