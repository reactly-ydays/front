import React, { useContext, useState } from "react"
import { v4 as uuidv4 } from "uuid"
import { useDrop } from "react-dnd"
import { useForm } from "react-hook-form"
import firebase from "../../../../firebase/firebase"
import { TablesContext } from "../../../../context/tablesContext"
import ModalTemplate from "../../../../utils/ModalTemplate"
import { UserContext } from "../../../../context/userContext"
import { capitalize, getRandomColor, priorityStatus } from "../../../../utils/Utils"
import { TaskType } from "../../../../models/Types"

type PropsType = {
    onDrop: any,
    children: any,
    subTableId: any,
    table: any,
    tableIndex: number
}

const DropWrapper: React.FC<PropsType> = ({ onDrop, children, table, subTableId, tableIndex }) => {
    const { userState } = useContext(UserContext)
    const { tablesState, tablesDispatch } = useContext(TablesContext)
    const [showModal, setShowModal] = useState<boolean>(false)

    const db = firebase.firestore()

    const { handleSubmit, register, errors } = useForm()

    const onSubmit = ({ title, description, priority, label, estimate }: any) => {
        const _id = uuidv4()
        const user = {
            id: userState.user?._id,
            name: `${userState.user!.firstName} ${userState.user!.lastName}`,
            job: `${capitalize(userState.user!.team)} - ${userState.user!.job}`,
            image: userState.user!.image
        }
        const labelIndex = table.tasks.findIndex((t: TaskType) => capitalize(t.label) === capitalize(label))
        const labelColor = (labelIndex >= 0) ? table.tasks[labelIndex].labelColor : getRandomColor()

        db.collection("tables").doc(table._id).set({
            tasks: [...table.tasks, { _id, title, description, user, priority, label, labelColor, estimate, subTableId }]
        }, { merge: true })

        tablesDispatch({
            type: "ADD_TASK",
            payload: {
                _id,
                title,
                description,
                subTableId,
                user,
                priority,
                label,
                labelColor,
                estimate,
                tableIndex
            }
        })
        setShowModal(false)
    }

    const [{ isOver }, drop] = useDrop({
        accept: "ITEM",
        drop: (item, monitor) => {
            onDrop(item, monitor, subTableId)
        },
        collect: monitor => ({
            isOver: monitor.isOver()
        })
    })

    return (
        <div ref={drop} className={`h-full task  ${isOver ? "bg-gray-200 bg-opacity-50" : ""}`}>
            {!children.props.children.length && <p className="px-1.5 py-1.5 invisible">a</p>}
            {React.cloneElement(children, { isOver })}
            <div onClick={() => setShowModal(true)} className="bg-white-normal cursor-pointer w-full mt-2 text-sm sm:text-md text-blue-500 font-bold">
                <div className="flex items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                    <p>Ajouter une tâche</p>
                </div>
            </div>

            {showModal ? (
                <ModalTemplate title="Ajouter une tâche" showModal={setShowModal}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="relative py-2 flex-auto">
                            {/* Header */}
                            <p className="pt-2 text-lg">Titre</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="title"
                                placeholder="Titre"
                                defaultValue=""
                                autoFocus
                                ref={register && register({ required: true })}
                                style={{ borderColor: errors.title && "#f02849" }}
                            />
                            {errors.title && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un titre</p>}
                            <p className="pt-2 text-lg">Description</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="description"
                                defaultValue=""
                                placeholder="Description"
                                ref={register && register({ required: false })}
                            />
                            <p className="pt-2 text-lg">Label</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="label"
                                placeholder=""
                                defaultValue=""
                                ref={register && register({ required: false })}
                            />
                            <p className="pt-2 text-lg">Priorité</p>
                            <select defaultValue="" name="priority" ref={register && register({ required: false })} className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none cursor-pointer">
                                {priorityStatus.map(p => <option value={p}>{p}</option>)}
                            </select>
                            <p className="pt-2 text-lg">Estimation</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="number"
                                name="estimate"
                                placeholder=""
                                defaultValue=""
                                ref={register && register({ required: false })}
                            />
                        </div>
                        {/* footer */}
                        <button className="bg-blue-600 cursor-pointer w-full mt-1.2 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Ajouter la tâche</button>
                    </form>
                </ModalTemplate>
            ) : null}
        </div>
    )
}

export default DropWrapper