/* eslint-disable react/jsx-fragments */
import React, { useContext, useRef, useState } from "react"
import { useDrag, useDrop } from "react-dnd"
import { useForm } from "react-hook-form"
import CheckBoxIcon from "../../../../assets/img/kanban/checkbox.svg"
import firebase from "../../../../firebase/firebase"
import ModalTemplate from "../../../../utils/ModalTemplate"
import { TablesContext } from "../../../../context/tablesContext"
import PriorityIcon from "./PriorityIcon"
import { capitalize, getRandomColor, priorityStatus } from "../../../../utils/Utils"
import { TaskType } from "../../../../models/Types"

type PropsType = {
    item: any,
    index: any,
    tableIndex: number
}

const Task: React.FC<PropsType> = ({ item, index, tableIndex }) => {
    const ref: any = useRef(null)
    const { tablesState, tablesDispatch } = useContext(TablesContext)
    const [showModalEdit, setShowModalEdit] = useState<boolean>(false)
    const [title, setTitle] = useState<string>(item.title)
    const [description, setDescription] = useState<string>(item.description)
    const [user, setUser] = useState<string>(item.user)
    const [priority, setPriority] = useState<string>(item.priority)
    const [estimate, setEstimate] = useState<string>(item.estimate)
    const [label, setLabel] = useState<string>(item.label)
    const [labelColor, setLabelColor] = useState<string>(item.labelColor)

    const db = firebase.firestore()

    const { handleSubmit, register, errors } = useForm()

    const [, drop] = useDrop({
        accept: "ITEM",
        hover(itm: any, monitor: any) {
            if (!ref.current) {
                return
            }
            const dragIndex = itm.index
            const hoverIndex = index

            if (dragIndex === hoverIndex) {
                return
            }

            const hoveredRect = ref.current.getBoundingClientRect()
            const hoverMiddleY = (hoveredRect.bottom - hoveredRect.top) / 2
            const mousePosition = monitor.getClientOffset()
            const hoverClientY = mousePosition.y - hoveredRect.top

            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }

            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }
            itm.index = hoverIndex
        }
    })

    const [{ isDragging }, drag] = useDrag({
        type: "ITEM",
        item: { type: "ITEM", ...item, index },
        collect: monitor => ({
            isDragging: monitor.isDragging()
        })
    })

    drag(drop(ref))

    const taskEdit = async () => {
        const tableId = tablesState.tables[tableIndex]._id
        const arrTmp = tablesState.tables
        const indexTmp = arrTmp[tableIndex].tasks.findIndex(t => t._id === item._id)
        const labelIndex = tablesState.tables[tableIndex].tasks.findIndex((t: TaskType) => capitalize(t.label) === capitalize(label))
        const checkLabelColor: any = (labelIndex >= 0) ? tablesState.tables[tableIndex].tasks[labelIndex].labelColor : getRandomColor()

        arrTmp[tableIndex].tasks[indexTmp].title = title
        arrTmp[tableIndex].tasks[indexTmp].description = description
        arrTmp[tableIndex].tasks[indexTmp].priority = priority
        arrTmp[tableIndex].tasks[indexTmp].label = label
        arrTmp[tableIndex].tasks[indexTmp].labelColor = checkLabelColor
        arrTmp[tableIndex].tasks[indexTmp].estimate = estimate

        db.collection("tables").doc(tableId).update({
            tasks: arrTmp[tableIndex].tasks
        })
        tablesDispatch({
            type: "EDIT_TASK",
            payload: {
                tableIndex,
                task: { ...item, title, description, user, priority, label, labelColor: getRandomColor(), estimate }
            }
        })
        setLabelColor(checkLabelColor)
        setShowModalEdit(false)
    }

    const handlerDeleteTask = () => {
        const tableId = tablesState.tables[tableIndex]._id
        const arrTmp = tablesState.tables[tableIndex]
        const arrTmp2 = arrTmp.tasks.filter(t => t._id !== item._id)

        db.collection("tables").doc(tableId).update({
            tasks: arrTmp2
        })

        tablesDispatch({
            type: "DELETE_TASK",
            payload: {
                tableIndex,
                taskId: item._id
            }
        })
    }

    return (
        <>
            <div ref={ref} onClick={() => setShowModalEdit(true)} className="bg-gray-200 bg-opacity-50 px-1.5 py-1.5 shadow z-49 rounded cursor-pointer">
                <p className="item-title">{item.title}</p>
                {label && (
                    <div className="mt-1 mb-1.5 flex justify-between">
                        <div className="rounded px-1 text-white-normal font-bold text-sm" style={{ backgroundColor: labelColor }}>
                            <p>{label}</p>
                        </div>
                    </div>
                )}
                <div className="mt-1 flex justify-between">
                    <div className="text-gray-400 flex justify-between space-x-1">
                        <img src={CheckBoxIcon} alt="checkbox" className="w-5 h-6" />
                        {priority && <PriorityIcon priority={priority} />}
                        {estimate && (
                            <div className="bg-gray-200 rounded-3xl flex items-center text-center">
                                <p className="w-6 text-gray-700 text-sm font-medium">{estimate}</p>
                            </div>
                        )}
                    </div>
                    <img src={`${item.user.image}`} className="w-9 h-9 sm:w-6 sm:h-6 rounded-full" alt="user profil" width="40" height="40" />
                </div>
            </div>
            {/* Editer une tâche */}
            {showModalEdit ? (
                <ModalTemplate title="Editer la tâche" showModal={setShowModalEdit}>
                    <form onSubmit={handleSubmit(taskEdit)}>
                        <div className="relative py-2 flex-auto">
                            {/* Header */}
                            <p className="pt-2 text-lg">Titre</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="title"
                                placeholder="Titre"
                                defaultValue={title}
                                onChange={e => setTitle(e.target.value)}
                                autoFocus
                                ref={register && register({ required: true })}
                                style={{ borderColor: errors.title && "#f02849" }}
                            />
                            {errors.title && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un titre</p>}
                            <p className="pt-2 text-lg">Description</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="description"
                                placeholder="Description"
                                defaultValue={description}
                                onChange={e => setDescription(e.target.value)}
                                ref={register && register({ required: false })}
                            />
                            <p className="pt-2 text-lg">Label</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="label"
                                placeholder=""
                                defaultValue={label}
                                onChange={e => setLabel(e.target.value)}
                                ref={register && register({ required: false })}
                            />
                            <p className="pt-2 text-lg">Priorité</p>
                            <select defaultValue={priority} name="priority" onChange={e => setPriority(e.target.value)} ref={register && register({ required: false })} className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none cursor-pointer">
                                {priorityStatus.map(p => <option value={p}>{p}</option>)}
                            </select>
                            <p className="pt-2 text-lg">Estimation</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="number"
                                name="estimate"
                                placeholder=""
                                defaultValue={estimate}
                                onChange={e => setEstimate(e.target.value)}
                                ref={register && register({ required: false })}
                            />

                            <div onClick={() => handlerDeleteTask()} className="bg-white-normal cursor-pointer w-full my-2 text-sm sm:text-md text-gray-500 hover:text-red-500 font-bold">
                                <div className="flex items-center space-x-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                    </svg>
                                    <p>Supprimer la tâche</p>
                                </div>
                            </div>
                        </div>
                        {/* footer */}
                        <button className="bg-blue-600 cursor-pointer w-full mt-1 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Enregistrer</button>
                    </form>
                </ModalTemplate>
            ) : null}
        </>
    )
}

export default Task