import React from "react"
import { useWindowResolution } from "../../../../hooks/CustomHooks"

const NoKanban: React.FC = () => {
    const { width } = useWindowResolution()

    return (
        <div className="lg:pt-7 pb-7">
            {(width > 640) && <h2 className="pt-12 mb-1 ml-1.5 sm:ml-2 text-lg lg:text-2xl font-bold text-gray-500">Pas de Tableau</h2>}
        </div>
    )
}

export default NoKanban