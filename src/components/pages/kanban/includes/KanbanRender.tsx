import React, { useContext, useState } from "react"
import { v4 as uuidv4 } from "uuid"
import { useHistory } from "react-router"
import { useForm } from "react-hook-form"
import firebase from "../../../../firebase/firebase"
import { TablesContext } from "../../../../context/tablesContext"
import { TableType } from "../../../../models/Types"
import ModalTemplate from "../../../../utils/ModalTemplate"
import Col from "./Col"
import DropWrapper from "./DropWrapper"
import Task from "./Task"

type PropsType = {
    table: TableType,
    tableIndex: number
}

const KanbanRender: React.FC<PropsType> = ({ table, tableIndex }) => {
    const { tablesState, tablesDispatch } = useContext(TablesContext)
    const [showModalCol, setShowModalCol] = useState<boolean>(false)
    const [showModalDeleteCol, setShowModalDeleteCol] = useState<boolean>(false)
    const [showModalEditCol, setShowModalEditCol] = useState<boolean>(false)
    const [deleteSubTableId, setDeleteSubTableId] = useState<string>("")
    const [editSubTableName, setEditSubTableName] = useState<string>("")
    const [showModalDeleteTable, setShowModalDeleteTable] = useState<boolean>(false)
    const [showModalEditTable, setShowModalEditTable] = useState<boolean>(false)
    const [deleteTableId, setDeleteTableId] = useState<string>("")
    const [editTableName, setEditTableName] = useState<string>("")

    const db = firebase.firestore()

    const { handleSubmit, register, errors } = useForm()
    const history = useHistory()

    const addSubTable = async ({ subTableName }: any) => {
        const _id = uuidv4()
        db.collection("tables").doc(table._id).set({
            subTable: [...table.subTable, { _id, name: subTableName, tableId: table._id }]
        }, { merge: true })
        tablesDispatch({
            type: "ADD_SUB_TABLE",
            payload: { _id, name: subTableName, tableId: table._id, tableIndex }
        })
        setShowModalCol(false)
    }

    const subTableEdit = async ({ subTableName }: any) => {
        const arrTmp = table
        const index = arrTmp.subTable.findIndex(t => t._id === deleteSubTableId)
        arrTmp.subTable[index].name = subTableName

        db.collection("tables").doc(table._id).update({
            subTable: arrTmp.subTable
        })
        tablesDispatch({
            type: "EDIT_SUB_TABLE",
            payload: { _id: deleteSubTableId, name: subTableName, tableId: table._id, tableIndex }
        })
        setDeleteSubTableId("")
        setShowModalEditCol(false)
    }

    const tableEdit = async ({ tableName }: any) => {
        const arrTmp = table
        arrTmp.name = tableName

        db.collection("tables").doc(table._id).update({
            name: arrTmp.name
        })
        tablesDispatch({
            type: "EDIT_TABLE",
            payload: { name: arrTmp.name, tableIndex }
        })

        setDeleteTableId("")
        setShowModalEditTable(false)
    }

    const onDrop = (item: any, monitor: any, id: any) => {
        const arrTmp = table
        const index = arrTmp.tasks.findIndex(t => t._id === item._id)
        arrTmp.tasks[index].subTableId = id

        db.collection("tables").doc(table._id).update({
            tasks: arrTmp.tasks
        })

        tablesDispatch({
            type: "MOVE_TASK",
            payload: { task: item, subTableId: id, tableIndex }
        })
    }

    const handlerDeleteCol = () => {
        const arrTmp = table
        const arrTmp2 = arrTmp.subTable.filter(t => t._id !== deleteSubTableId)
        const arrTmp3 = arrTmp.tasks.filter(t => t.subTableId !== deleteSubTableId)

        db.collection("tables").doc(table._id).update({
            tasks: arrTmp3,
            subTable: arrTmp2
        })

        tablesDispatch({
            type: "DELETE_SUB_TABLE",
            payload: { subTableId: deleteSubTableId, tableIndex }
        })

        setDeleteSubTableId("")
        setShowModalDeleteCol(false)
    }

    const handlerDeleteTable = () => {
        db.collection("tables").doc(table._id).delete()
        tablesDispatch({
            type: "DELETE_TABLE",
            payload: {
                tableId: table._id
            }
        })

        setDeleteTableId("")
        setShowModalDeleteTable(false)
    }

    return (
        <div className="w-max pl-2 h-screen lg:pt-7 pb-7 pl-1.5 sm:pl-2 pr-2 overflow-x-auto">
            <div className="group pt-12 mb-1 flex items-center space-x-1">
                <h2 className="text-lg lg:text-2xl font-bold text-gray-500">{table.name}</h2>
                <div className="font-medium opacity-0 group-hover:opacity-100 flex items-center space-x-1">
                    <div onClick={() => { setEditTableName(table.name); setDeleteTableId(table._id); setShowModalEditTable(true) }} className="cursor-pointer text-gray-500 hover:text-blue-500">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                        </svg>
                    </div>
                    <div onClick={() => { setShowModalDeleteTable(true); setDeleteTableId(table._id) }} className="cursor-pointer text-gray-500 hover:text-red-500">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                        </svg>
                    </div>
                </div>
            </div>

            <div className="flex space-x-1.2 mt-2">
                {
                    tablesState.tables[tableIndex].subTable.map((s: any, index: number) => {
                        return (
                            <div key={s._id} className="bg-white-normal px-2 py-1.5 shadow z-49 rounded h-full task group">
                                <div className="flex justify-between items-center">
                                    <h2 className="font-medium mb-2 text-sm text-blue">{s.name}</h2>
                                    <div className="font-medium mb-2 opacity-0 group-hover:opacity-100 flex items-center space-x-1">
                                        <div onClick={() => { setEditSubTableName(s.name); setDeleteSubTableId(s._id); setShowModalEditCol(true) }} className="cursor-pointer text-gray-500 hover:text-blue-500">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                            </svg>
                                        </div>
                                        <div onClick={() => { setShowModalDeleteCol(true); setDeleteSubTableId(s._id) }} className="cursor-pointer text-gray-500 hover:text-red-500">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <DropWrapper onDrop={onDrop} table={table} subTableId={s._id} tableIndex={tableIndex}>
                                    <Col>
                                        {tablesState.tables[tableIndex].tasks
                                            .filter((t: any) => t.subTableId === s._id)
                                            .map((t: any, idx: number) => <Task key={t._id} item={t} index={idx} tableIndex={tableIndex} />)
                                        }
                                    </Col>
                                </DropWrapper>
                            </div>
                        )
                    })
                }
                <div>
                    <div onClick={() => setShowModalCol(true)} className="bg-green-500 cursor-pointer w-full mb-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2 text-center">
                        <div className="flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                            </svg>
                            <p>Ajouter une colonne</p>
                        </div>
                    </div>
                </div>
            </div>

            {/* Ajouter une colonne */}
            {showModalCol ? (
                <ModalTemplate title="Ajouter une colonne" showModal={setShowModalCol}>
                    <form onSubmit={handleSubmit(addSubTable)}>
                        <div className="relative py-2 flex-auto">
                            {/* Header */}
                            <p className="pt-2 text-lg">Nom de la colonne</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="subTableName"
                                placeholder="Nom de la colonne"
                                defaultValue=""
                                autoFocus
                                ref={register && register({ required: true })}
                                style={{ borderColor: errors.subTableName && "#f02849" }}
                            />
                            {errors.subTableName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un nom de colonne</p>}
                        </div>
                        {/* footer */}
                        <button className="bg-blue-600 cursor-pointer w-full mt-1.2 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Ajouter la colonne</button>
                    </form>
                </ModalTemplate>
            ) : null}

            {/* Editer une colonne */}
            {showModalEditCol ? (
                <ModalTemplate title="Editer une colonne" showModal={setShowModalEditCol}>
                    <form onSubmit={handleSubmit(subTableEdit)}>
                        <div className="relative py-2 flex-auto">
                            {/* Header */}
                            <p className="pt-2 text-lg">Nom de la colonne</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="subTableName"
                                placeholder="Nom de la colonne"
                                defaultValue={editSubTableName}
                                onChange={e => setEditSubTableName(e.target.value)}
                                autoFocus
                                ref={register && register({ required: true })}
                                style={{ borderColor: errors.subTableName && "#f02849" }}
                            />
                            {errors.subTableName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un nom de colonne</p>}
                        </div>
                        {/* footer */}
                        <button className="bg-blue-600 cursor-pointer w-full mt-1.2 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Enregistrer</button>
                    </form>
                </ModalTemplate>
            ) : null}

            {/* Supprimer une colonne */}
            {showModalDeleteCol ? (
                <ModalTemplate title="Supprimer la colonne" showModal={setShowModalDeleteCol}>
                    <>
                        <div className="relative pt-2 flex-auto">
                            {/* Header */}
                            <p className="py-1.2 text-lg">Êtes-vous sûr de vouloir supprimer cette colonne ?</p>
                        </div>
                        {/* footer */}
                        <div className="items-center mb-2 flex">
                            <button className="bg-gray-300 cursor-pointer w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2 mr-1" onClick={() => setShowModalDeleteCol(false)}>Annuler</button>
                            <button className="bg-red-error cursor-pointer w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" onClick={() => handlerDeleteCol()}>Supprimer</button>
                        </div>
                    </>
                </ModalTemplate>
            ) : null}

            {/* Editer une table */}
            {showModalEditTable ? (
                <ModalTemplate title="Editer une table" showModal={setShowModalEditTable}>
                    <form onSubmit={handleSubmit(tableEdit)}>
                        <div className="relative py-2 flex-auto">
                            {/* Header */}
                            <p className="pt-2 text-lg">Nom de la table</p>
                            <input
                                className="w-full bg-blue-100 sm:bg-white-normal my-0.5 p-2 text-base border border-gray-300 rounded-md font-medium outline-none"
                                type="text"
                                name="tableName"
                                placeholder="Nom de la table"
                                defaultValue={editTableName}
                                onChange={e => setEditTableName(e.target.value)}
                                autoFocus
                                ref={register && register({ required: true })}
                                style={{ borderColor: errors.tableName && "#f02849" }}
                            />
                            {errors.tableName && <p className="text-sm text-red-error mb-1 pl-1">Veuillez saisir un nom de table</p>}
                        </div>
                        {/* footer */}
                        <button className="bg-blue-600 cursor-pointer w-full mt-1.2 mb-1.5 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" type="submit">Enregistrer</button>
                    </form>
                </ModalTemplate>
            ) : null}

            {/* Supprimer une table */}
            {showModalDeleteTable ? (
                <ModalTemplate title="Supprimer la table" showModal={setShowModalDeleteCol}>
                    <>
                        <div className="relative pt-2 flex-auto">
                            {/* Header */}
                            <p className="py-1.2 text-lg">Êtes-vous sûr de vouloir supprimer cette table ?</p>
                        </div>
                        {/* footer */}
                        <div className="items-center mb-2 flex">
                            <button className="bg-gray-300 cursor-pointer w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2 mr-1" onClick={() => setShowModalDeleteTable(false)}>Annuler</button>
                            <button className="bg-red-error cursor-pointer w-full mt-1.2 rounded-md text-sm sm:text-md text-white-normal font-bold p-1.2" onClick={() => handlerDeleteTable()}>Supprimer</button>
                        </div>
                    </>
                </ModalTemplate>
            ) : null}
        </div>
    )
}

export default KanbanRender