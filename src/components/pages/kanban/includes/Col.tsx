import React from "react"

type PropsType = {
    children: any
}

const Col: React.FC<PropsType> = ({ children }) => (
    <div className="task flex flex-col space-y-1.2" style={{ width: "270px" }}>
        {children}
    </div>
)

export default Col