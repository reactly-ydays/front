import React, { useContext, useState } from "react"
import { useHistory } from "react-router"
import ScrollContainer from "react-indiana-drag-scroll"
import { DndProvider } from "react-dnd"
import { HTML5Backend } from "react-dnd-html5-backend"
import firebase from "../../../firebase/firebase"
import { checkUserAuth } from "../../../api/auth"
import { useWindowResolution } from "../../../hooks/CustomHooks"
import Loading from "../../../utils/Loading"
import { useComponentWillMount } from "../../../utils/Utils"
import NoKanban from "./includes/NoKanban"
import VerticalBar from "./includes/VerticalBar"
import { TablesContext } from "../../../context/tablesContext"
import { UserContext } from "../../../context/userContext"
import KanbanRender from "./includes/KanbanRender"
import { TableType } from "../../../models/Types"

type PropsType = {
    match: any
}

const Kanban: React.FC<PropsType> = ({ match }) => {
    const { id } = match.params
    const history = useHistory()
    const { width } = useWindowResolution()
    const { tablesState, tablesDispatch } = useContext(TablesContext)
    const { userState } = useContext(UserContext)
    const [tableIndex, setTableIndex] = useState<number>(0)
    const [tableId, setTableId] = useState<string>("")
    const [isLoading, setIsLoading] = useState<boolean>(true)
    const [redirect, setRedirect] = useState<boolean>(false)
    const [openMenu, setOpenMenu] = useState<boolean>(true)

    const db = firebase.firestore()

    document.body.style.backgroundColor = (width <= 640) ? "#e0e0e0" : "#F1F1F1"

    useComponentWillMount(async () => {
        if (isLoading) {
            if (checkUserAuth()) {
                db.collection("tables").where("teamId", "==", userState.user?.teamId).onSnapshot(snap => {
                    const tables: any = []
                    snap.forEach(doc => tables.push({ ...doc.data(), _id: doc.id }))

                    tablesDispatch({
                        type: "INIT_TABLE",
                        payload: tables
                    })

                    if (id === undefined || id === null) {
                        if (tables.length) {
                            setTableId(tables[tableIndex]._id)
                            history.push(`/kanban/${tableId}`)
                        }
                    } else {
                        const index = tables.findIndex((t: TableType) => t._id === id)
                        if (tables.length) {
                            if (index > -1) {
                                setTableIndex(index)
                                setTableId(tables[index]._id)
                                history.push(`/kanban/${tableId}`)
                            } else {
                                setRedirect(true)
                            }
                        }
                    }
                    setIsLoading(false)
                })
            } else {
                setIsLoading(false)
            }
        }
    })

    const renderKanban = () => {
        if (tablesState.tables.length > 0) {
            return (
                <ScrollContainer className="scroll-container" ignoreElements=".task" hideScrollbars={false}>
                    <div className={`w-max pl-2 h-screen ${!openMenu ? "ml-44" : "ml-12"}`}>
                        <DndProvider backend={HTML5Backend}>
                            <KanbanRender table={tablesState.tables[tableIndex]} tableIndex={tableIndex} />
                        </DndProvider>
                    </div>
                </ScrollContainer>
            )
        } else {
            return (
                <div>
                    <NoKanban />
                </div>
            )
        }
    }

    if (redirect) window.location.replace("/kanban")
    if (isLoading) return <Loading />
    else {
        return (
            <div className="flex">
                <VerticalBar tableIndex={tableIndex} setTableIndex={setTableIndex} setOpenMenu={setOpenMenu} />
                {renderKanban()}
            </div>
        )
    }
}

export default Kanban