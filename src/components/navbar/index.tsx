import React, { useContext, useEffect, useState } from "react"
import { useLocation } from "react-router"
import { UserContext } from "../../context/userContext"
import { useWindowResolution } from "../../hooks/CustomHooks"
import Navbar from "./includes/Navbar"
import NavbarMobile from "./includes/NavbarMobile"

const NavbarRender: React.FC = () => {
    const [pathname, setPathname] = useState<string>("/")
    const { width } = useWindowResolution()
    const { userState } = useContext(UserContext)

    useEffect(() => {
        setPathname(location.pathname)
    }, [location])

    return (width > 1024) ? <Navbar /> : <NavbarMobile />
}

export default NavbarRender