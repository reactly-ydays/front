import React from "react"
import { withRouter } from "react-router-dom"
import { toast, ToastContainer } from "react-toastify"
import NavbarRender from "./navbar"
import Routes from "../routes/routes"
import UserProvider from "../context/userContext"
import PostsProvider from "../context/postsContext"
import { DEBUG } from "../utils/Utils"
import "react-toastify/dist/ReactToastify.css"
import "../assets/css/Notification.css"
import TablesProvider from "../context/tablesContext"

type PropsType = {
  location : any
}

const App: React.FC<PropsType> = ({ location }) => (
    <UserProvider>
        <PostsProvider>
            <TablesProvider>
                <meta name="theme-color" content={DEBUG ? "#FFA748" : "#4848FF"} />
                {(location.pathname !== "/login" && location.pathname !== "/register") && <NavbarRender />}
                <ToastContainer
                    position={toast.POSITION.TOP_RIGHT}
                    autoClose={3000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    draggable
                />
                <Routes />
            </TablesProvider>
        </PostsProvider>
    </UserProvider>
)

export default withRouter(App)
