import React, { useReducer } from "react"
import { initState, postsReducer } from "../reducers/postsReducers"
import { PostType } from "../models/Types"

const defaultValueType: PostsContextType = {
    postsState: initState,
    postsDispatch: () => null
}

type PropsType = {
    children: any
}

export interface PostsContextType {
    postsState: { posts: [PostType] | null, hasNextPage: boolean, loadMorePost: boolean },
    postsDispatch: any
}

export const PostsContext = React.createContext<PostsContextType>(defaultValueType)

const PostsProvider: React.FC<PropsType> = ({ children }) => {
    const [postsState, postsDispatch] = useReducer(postsReducer, initState)

    return (
        <PostsContext.Provider value={{ postsState, postsDispatch }}>
            { children }
        </PostsContext.Provider>
    )
}

export default PostsProvider
