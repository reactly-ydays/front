import React from "react"
import { shallow } from "enzyme"
import { BrowserRouter } from "react-router-dom"
import { cleanup, render, fireEvent, act } from "@testing-library/react"
import Register from "../../../components/pages/auth/includes/Register"
import { registerRequest, allTeamRequest } from "../../../components/pages/auth/includes/Requests"

jest.mock("../../../components/pages/auth/includes/Requests")

describe("Register", () => {
    const emailMockValue = "Test@123"
    const passwordMockValue = "test"
    const firstNameMockValue = "test"
    const lastNameMockValue = "test"
    const jobMockValue = "test"

    afterEach(cleanup)

    test("should watch input correctly", async () => {
        allTeamRequest.mockImplementationOnce(() => Promise.resolve({
            data: {
                status: "success",
                data: ["test"]
            }
        }))
        const { container } = render(
            <BrowserRouter>
                <Register />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const confirmPassword = container.querySelector("input[name='password']")
        const firstName = container.querySelector("input[name='firstName']")
        const lastName = container.querySelector("input[name='firstName']")
        const job = container.querySelector("input[name='job']")

        const submitButton = container.querySelector("input[type='submit']")

        fireEvent.input(email!, {
            target: {
                value: emailMockValue
            }
        })
        fireEvent.input(password!, {
            target: {
                value: passwordMockValue
            }
        })
        fireEvent.input(confirmPassword!, {
            target: {
                value: passwordMockValue
            }
        })
        fireEvent.input(firstName!, {
            target: {
                value: firstNameMockValue
            }
        })
        fireEvent.input(lastName!, {
            target: {
                value: lastNameMockValue
            }
        })
        fireEvent.input(job!, {
            target: {
                value: jobMockValue
            }
        })

        await act(async () => {
            fireEvent.submit(submitButton!)
        })

        expect(email.value).toEqual(emailMockValue)
        expect(password.value).toEqual(passwordMockValue)
        expect(confirmPassword.value).toEqual(passwordMockValue)
        expect(firstName.value).toEqual(firstNameMockValue)
        expect(lastName.value).toEqual(lastNameMockValue)
        expect(job.value).toEqual(jobMockValue)
    })

    test("should display correct error message for password miss match", async () => {
        allTeamRequest.mockImplementationOnce(() => Promise.resolve({
            data: {
                status: "success",
                data: ["test"]
            }
        }))
        const { container } = render(
            <BrowserRouter>
                <Register />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const confirmPassword = container.querySelector("input[name='password']")
        const firstName = container.querySelector("input[name='firstName']")
        const lastName = container.querySelector("input[name='firstName']")
        const team = container.querySelector("select[name='team']")
        const job = container.querySelector("input[name='job']")

        const submitButton = container.querySelector("input[type='submit']")

        fireEvent.input(email!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(password!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(confirmPassword!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(firstName!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(lastName!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(team!, {
            target: {
                value: ""
            }
        })
        fireEvent.input(job!, {
            target: {
                value: ""
            }
        })

        await act(async () => {
            fireEvent.submit(submitButton!)
        })

        expect(container.textContent).toMatch(/Veuillez saisir un email/)
        expect(container.textContent).toMatch(/Veuillez saisir un mot de passe/)
        expect(container.textContent).toMatch(/Veuillez saisir un prénom/)
        expect(container.textContent).toMatch(/Veuillez saisir un nom/)
        expect(container.textContent).toMatch(/Veuillez saisir une équipe/)
        expect(container.textContent).toMatch(/Veuillez saisir un métier/)
        expect(container.textContent).toMatch(/La confirmation doit être identique au mot de passe/)
    })

    test("Should submit form successfully", async () => {
        registerRequest.mockImplementationOnce(() => Promise.resolve({
            data: {
                success: "success",
                data: []
            }
        }))

        allTeamRequest.mockImplementationOnce(() => Promise.resolve({
            data: {
                status: "success",
                data: ["test"]
            }
        }))
        const { container } = render(
            <BrowserRouter>
                <Register />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const confirmPassword = container.querySelector("input[name='password']")
        const firstName = container.querySelector("input[name='firstName']")
        const lastName = container.querySelector("input[name='firstName']")
        const team = container.querySelector("select[name='team']")
        const job = container.querySelector("input[name='job']")

        const submitButton = container.querySelector("input[type='submit']")

        fireEvent.input(email!, {
            target: {
                value: emailMockValue
            }
        })
        fireEvent.input(password!, {
            target: {
                value: passwordMockValue
            }
        })
        fireEvent.input(confirmPassword!, {
            target: {
                value: passwordMockValue
            }
        })
        fireEvent.input(firstName!, {
            target: {
                value: firstNameMockValue
            }
        })
        fireEvent.input(lastName!, {
            target: {
                value: lastNameMockValue
            }
        })
        fireEvent.input(job!, {
            target: {
                value: jobMockValue
            }
        })

        fireEvent.input(team!, {
            target: {
                value: "test"
            }
        })

        await act(async () => {
            fireEvent.submit(submitButton!)
        })

        expect(allTeamRequest).toHaveBeenCalledWith()
        // expect(registerRequest).toHaveBeenCalledWith(emailMockValue, passwordMockValue, firstNameMockValue, lastNameMockValue, jobMockValue, "test")
    })

    test("Should submit form error", async () => {
        registerRequest.mockImplementationOnce(() => Promise.resolve({
            data: {
                success: "success",
                data: []
            }
        }))

        allTeamRequest.mockImplementationOnce(() => Promise.resolve({
            data: {
                success: "success",
                data: ["test"]
            }
        }))

        const { container } = render(
            <BrowserRouter>
                <Register />
            </BrowserRouter>
        )

        const email = container.querySelector("input[name='email']")
        const password = container.querySelector("input[name='password']")
        const confirmPassword = container.querySelector("input[name='password']")
        const firstName = container.querySelector("input[name='firstName']")
        const lastName = container.querySelector("input[name='firstName']")
        const team = container.querySelector("select[name='team']")
        const job = container.querySelector("input[name='job']")

        const submitButton = container.querySelector("input[type='submit']")

        act(() => {
            fireEvent.input(email!, {
                target: {
                    value: emailMockValue
                }
            })
            fireEvent.input(password!, {
                target: {
                    value: passwordMockValue
                }
            })
            fireEvent.input(confirmPassword!, {
                target: {
                    value: passwordMockValue
                }
            })
            fireEvent.input(firstName!, {
                target: {
                    value: firstNameMockValue
                }
            })
            fireEvent.input(lastName!, {
                target: {
                    value: lastNameMockValue
                }
            })
            fireEvent.input(job!, {
                target: {
                    value: jobMockValue
                }
            })
            fireEvent.input(team!, {
                target: {
                    value: "test"
                }
            })
            fireEvent.submit(submitButton!)
        })
    })
})