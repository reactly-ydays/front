import React from "react"
import axios from "axios"
import { shallow } from "enzyme"
import { getPost } from "../../../api/ApiRequest"
import { checkUserAuth } from "../../../api/auth"
import SharedPost from "../../../components/pages/timelines/SharedPost"

jest.mock("axios")
jest.mock("../../../api/ApiRequest", () => ({
    getPost: jest.fn()
}))
jest.mock("../../../api/auth", () => ({
    checkUserAuth: jest.fn()
}))

describe("SharedPost", () => {
    const props = {
        match: {
            params: {
                postId: "test"
            }
        }
    }

    it("Should render SharedPost", () => {
        checkUserAuth.mockReturnValueOnce(false)
        const wrapper = shallow(<SharedPost {...props} />)
        expect(wrapper.contains(<div />)).toEqual(true)
    })
})
