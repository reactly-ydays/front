import React from "react"
import axios from "axios"
import { shallow } from "enzyme"
import { render } from "@testing-library/react"
import Timelines from "../../../components/pages/timelines"
import { getAllPost } from "../../../api/ApiRequest"
import { checkUserAuth } from "../../../api/auth"
import NoActualite from "../../../components/pages/timelines/includes/NoActualite"
import { PostsContext } from "../../../context/postsContext"
import { PostType } from "../../../models/Types"

jest.mock("axios")
jest.mock("../../../api/ApiRequest", () => ({
    getAllPost: jest.fn()
}))
jest.mock("../../../api/auth", () => ({
    checkUserAuth: jest.fn()
}))

describe("Timeline", () => {
    it("Should render Timeline", () => {
        checkUserAuth.mockReturnValueOnce(false)
        const wrapper = shallow(<Timelines />)
        expect(wrapper.contains(<NoActualite />)).toEqual(true)
    })
    it("Should render Timeline", () => {
        checkUserAuth.mockReturnValueOnce(true)
        getAllPost.mockImplementationOnce(() => Promise.resolve({
            data: {
                success: "success",
                data: [{
                    _id: "test",
                    user: {
                        id: "test",
                        name: "test",
                        job: "test",
                        image: ""
                    },
                    comments: [],
                    nbLike: 0,
                    description: "description",
                    images: null,
                    publicationDate: new Date(),
                    currentUserLiked: false
                }]
            }
        }))
        const postsDispatch = jest.fn()
        const postsState: { posts: [PostType] } = {
            posts: [{
                _id: "test",
                comments: [],
                user: {
                    id: "test",
                    name: "test",
                    job: "test",
                    image: ""
                },
                nbLike: 0,
                description: "C'est la description de mon post de test",
                images: undefined,
                publicationDate: new Date(),
                currentUserLiked: false
            }]
        }

        const { container } = render(
            <PostsContext.Provider value={{ postsState, postsDispatch }}>
                <Timelines />
            </PostsContext.Provider>
        )

        expect(container.textContent).toMatch(/C'est la description de mon post de test/)
    })
})
