import React from "react"
import axios from "axios"
import { shallow } from "enzyme"
import { checkUserAuth } from "../../../api/auth"
import Navbar from "../../../components/navbar/includes/Navbar"
import NavbarMobile from "../../../components/navbar/includes/NavbarMobile"
import NavbarRender from "../../../components/navbar"

jest.mock("axios")
jest.mock("../../../api/auth", () => ({
    checkUserAuth: jest.fn()
}))

describe("NavbarRender", () => {
    it("Should render NavbarRender", () => {
        checkUserAuth.mockReturnValueOnce(false)
        const wrapper = shallow(<NavbarRender />)
        expect(wrapper.contains(<NavbarMobile />)).toEqual(true)
        expect(wrapper.contains(<Navbar />)).toEqual(false)
    })
})
