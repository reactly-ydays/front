import React from "react"
import { toast, ToastContainer } from "react-toastify"
import { createMemoryHistory } from "history"
import { shallow } from "enzyme"
import App from "../components/App"
import UserProvider from "../context/userContext"
import PostsProvider from "../context/postsContext"
import Routes from "../routes/routes"
import NavbarRender from "../components/navbar"

jest.mock("axios")

describe("App", () => {
    const DEBUG = false
    const props = {
        location: createMemoryHistory().location
    }

    it("Should App render", () => {
        shallow(<App {...props} />)
    })

    describe("check routes", () => {
        let routes = "/"

        it("Should Login render", () => {
            routes = "/login"
            const wrapper = shallow(
                <UserProvider>
                    <PostsProvider>
                        <meta name="theme-color" content={DEBUG ? "#FFA748" : "#4848FF"} />
                        {(routes !== "/login" && routes !== "/register") && <NavbarRender />}
                        <ToastContainer
                            position={toast.POSITION.TOP_RIGHT}
                            autoClose={3000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            draggable
                        />
                        <Routes />
                    </PostsProvider>
                </UserProvider>
            )
            expect(wrapper.contains(<NavbarRender />)).toEqual(false)
        })

        it("Should Register render", () => {
            routes = "/register"
            const wrapper = shallow(
                <UserProvider>
                    <PostsProvider>
                        <meta name="theme-color" content={DEBUG ? "#FFA748" : "#4848FF"} />
                        {(routes !== "/login" && routes !== "/register") && <NavbarRender />}
                        <ToastContainer
                            position={toast.POSITION.TOP_RIGHT}
                            autoClose={3000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            draggable
                        />
                        <Routes />
                    </PostsProvider>
                </UserProvider>
            )
            expect(wrapper.contains(<NavbarRender />)).toEqual(false)
        })

        it("Should Other render", () => {
            routes = "/"
            const wrapper = shallow(
                <UserProvider>
                    <PostsProvider>
                        <meta name="theme-color" content={DEBUG ? "#FFA748" : "#4848FF"} />
                        {(routes !== "/login" && routes !== "/register") && <NavbarRender />}
                        <ToastContainer
                            position={toast.POSITION.TOP_RIGHT}
                            autoClose={3000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            draggable
                        />
                        <Routes />
                    </PostsProvider>
                </UserProvider>
            )
            expect(wrapper.contains(<NavbarRender />)).toEqual(true)
        })
    })

    describe("environment variable", () => {
        it("Should meta color prod", () => {
            const metaColorContent = "#4848FF"
            const wrapper = shallow(
                <UserProvider>
                    <PostsProvider>
                        <meta name="theme-color" content={DEBUG ? "#FFA748" : "#4848FF"} />
                        {(props.location.pathname !== "/login" && props.location.pathname !== "/register") && <NavbarRender />}
                        <ToastContainer
                            position={toast.POSITION.TOP_RIGHT}
                            autoClose={3000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            draggable
                        />
                        <Routes />
                    </PostsProvider>
                </UserProvider>
            )
            const container = wrapper.find("meta[name='theme-color']").props().content
            expect(container).toEqual(metaColorContent)
        })
    })
})
