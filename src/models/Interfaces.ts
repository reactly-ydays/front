export interface ISelectTeam {
    value: string,
    label: string
}